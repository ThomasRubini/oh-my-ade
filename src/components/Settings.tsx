import { TextField } from '@mui/material';
import { Component, ChangeEvent } from "react";

interface SettingsProps {
    url: string,
    onChange: (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void,
    error: boolean,
}

export class Settings extends Component<SettingsProps, any> {

    render() {
        return (
            <div id={'view'}>
                <label>
                    <div className='url-label'>Url du calendrier en ical (<a
                        href={'https://gitlab.com/nilsponsard/oh-my-ade/-/wikis/Comment-obtenir-l%E2%80%99url-du-calendrier'}>comment
                        l’obtenir</a>) :
                    </div>

                    <TextField error={this.props.error} fullWidth
                        sx={{ m: 1, width: { xs: '90%', sm: '90%', md: '75%', lg: '50%', xl: '40%' } }} type='text'
                        variant="standard"
                        label="URL du calendrier"
                        onChange={(event) => this.props.onChange(event)}
                        value={this.props.url} />
                </label>
            </div>
        );
    }
}