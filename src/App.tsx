import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Component, ChangeEvent } from 'react';
import './App.css';
import CalendarRefresh, { compactEnum, modes, } from './components/CalendarRefresh';
import { Settings } from './components/Settings';
import { CourseModel, getCachedCourses, getCourses, OffsetDay } from "./model/CalendarModel";
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { ModeIcon } from './components/ModeIcon';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import SettingsSharpIcon from '@mui/icons-material/SettingsSharp';
import RefreshIcon from '@mui/icons-material/Refresh';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import LinearProgress from '@mui/material/LinearProgress';
import Snackbar from '@mui/material/Snackbar';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import { getStatusMessage } from './utils/statusMessage';
import * as converter from './utils/dateConversions';

interface AppState {
    url: string,
    result: Map<string, Array<CourseModel>>,
    error: boolean,
    cached: boolean,
    loading: boolean,
    newCalendar: boolean,
    mode: modes,
    compact: compactEnum,
    showSnack: boolean;
    currentDay: string;
    settings: boolean;
}




export const theme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#3949ab',
        },
        secondary: {
            main: '#80cbc4',
        },
    },

});


function calculateSettings(mode: modes, day: string) {
    const rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
    const screenWidthRem = window.innerWidth / rem;
    const isLarger = screenWidthRem > 130;
    const isLarge = screenWidthRem > 105;
    const isMedium = screenWidthRem > 60;

    if (mode === modes.auto) {
        mode = modes.one;
        if (isMedium)
            mode = modes.three;
        if (isLarge)
            mode = modes.week;
    }

    let compact = compactEnum.no;
    if (!isLarger && mode === modes.week)
        compact = compactEnum.yes;
    if (!isMedium && mode === modes.three)
        compact = compactEnum.yes;


    let nbDays: number;

    switch (mode) {
        case modes.three:
            nbDays = 3;
            break;
        case modes.week:
            nbDays = 7;

            //-- Set at the begining of the week

            let selectedDate = converter.StringToDate(day);
            const dayOfWeek = selectedDate.getDay();
            let diff = selectedDate.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1);
            day = converter.DateToString(new Date(selectedDate.setDate(diff)));
            break;

        case modes.one:
        default:
            nbDays = 1;
            break;
    }


    return { day, nbDays, mode, compact };

}

class App extends Component<any, AppState> {

    constructor(props: any) {
        super(props);
        let url = localStorage.getItem('last_url');
        if (!url) {
            url = '';
        }

        this.state = {
            currentDay: converter.DateToString(new Date(Date.now())),
            showSnack: false,
            url,
            result: new Map<string, Array<CourseModel>>(),
            error: false,
            cached: false,
            loading: true,
            newCalendar: true,
            mode: modes.auto,
            compact: compactEnum.auto,
            settings: false
        };
    }

    async componentDidMount() {
        window.addEventListener('resize', () => {
            this.forceUpdate();
        });
        window.addEventListener('keydown', (ev) => this.handleKey(ev));
        await this.populate(false, true);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => {
            this.forceUpdate();
        });
        window.removeEventListener('keydown', (ev) => this.handleKey(ev));

    }


    handleKey(ev: KeyboardEvent) {

        const { nbDays } = calculateSettings(this.state.mode, this.state.currentDay);

        if (ev.key === "ArrowRight") {
            this.setState({ currentDay: OffsetDay(nbDays, this.state.currentDay) });
        }
        if (ev.key === "ArrowLeft") {
            this.setState({ currentDay: OffsetDay(-nbDays, this.state.currentDay) });
        }
    }


    async populate(forceRefresh = false, cache = false) {
        this.setState({ loading: true });

        if (cache) {
            let output = getCachedCourses();
            this.setState({ result: output.result, error: output.error, cached: output.status === 'cache' });
        }


        console.log("fetching : " + this.state.url);
        let output = await getCourses(this.state.url, forceRefresh);
        let result = output.result;
        let error = output.error;
        let cached = output.status === 'cache';
        console.log("using " + output.status + " ressource");
        this.setState({ result, error, cached, loading: false, showSnack: true });
    }

    onChangeHandler(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        let url = event.target.value;

        this.setState({ url, loading: true, error: false, cached: false }, () => {
            this.populate(true);
        });

    }

    handleClose(
        _: any,
        reason?: string,
    ) {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ showSnack: false });
    };

    render() {

        const { day, nbDays, mode, compact } = calculateSettings(this.state.mode, this.state.currentDay);

        if (this.state.url.length < 1 && !this.state.settings)
            this.setState({ settings: true });

        return (
            <ThemeProvider theme={theme}>
                <Box sx={{
                    backgroundColor: 'background.default',
                    minHeight: '100vh',
                    m: 0,
                    p: 0,
                }}>
                    <AppBar position="sticky">
                        <Toolbar color="primary">
                            {this.state.settings ?
                                <>
                                    <IconButton edge="start" color="inherit" aria-label="retour" sx={{ mr: 2 }}
                                        onClick={() => { this.setState({ settings: false }); }}>

                                        <ArrowBackIcon />
                                    </IconButton>

                                    <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                                        Paramètres
                                    </Typography>

                                </>
                                :
                                <>
                                    <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>

                                        {
                                            mode === modes.one ?
                                                <>
                                                    {converter.StringToDate(day).toLocaleDateString('fr', {
                                                        day: 'numeric',
                                                        month: 'short',
                                                        weekday: 'short'
                                                    })}
                                                </>
                                                :
                                                <>
                                                    Oh my ade
                                                </>
                                        }
                                    </Typography>
                                    <IconButton aria-label="jour précédent" onClick={() => {
                                        this.setState({ currentDay: OffsetDay(-nbDays, this.state.currentDay) });
                                    }}><NavigateBeforeIcon /></IconButton>
                                    <IconButton aria-label="Aujourd’hui" onClick={() => {
                                        this.setState({ currentDay: converter.DateToString(new Date(Date.now())) });
                                    }}>
                                        <CalendarTodayIcon />
                                    </IconButton>
                                    <IconButton aria-label="jour suivant" onClick={() => {
                                        this.setState({ currentDay: OffsetDay(nbDays, this.state.currentDay) });
                                    }}><NavigateNextIcon /></IconButton>
                                    <IconButton aria-label="changer l’affichage" onClick={() => {
                                        let mode = this.state.mode + 1;
                                        if (mode > modes.week)
                                            mode = modes.auto;

                                        this.setState({ mode });
                                    }}>
                                        <ModeIcon mode={this.state.mode} />
                                    </IconButton>
                                    <IconButton aria-label="actualiser" onClick={() => this.populate(true)}>
                                        <RefreshIcon />
                                    </IconButton>

                                    <IconButton edge="end" aria-label="paramètres" onClick={() => this.setState({ settings: true })}>
                                        <SettingsSharpIcon />
                                    </IconButton>
                                </>
                            }


                        </Toolbar>
                        {this.state.loading ?
                            <LinearProgress />
                            : <Box sx={{ height: "4px", width: "100%" }}></Box>}
                    </AppBar>
                    <div className="App">
                        <div className="App-content">
                            <div id={'content'}>
                                {
                                    this.state.settings ?

                                        (<Settings error={this.state.error} url={this.state.url}
                                            onChange={(event) => this.onChangeHandler(event)} />)
                                        :
                                        (<> <CalendarRefresh day={day} error={this.state.error}
                                            result={this.state.result}
                                            emptyUrl={this.state.url === ''} cached={this.state.cached}
                                            loading={this.state.loading} mode={mode}
                                            compact={compact}
                                            change={(direction) => {
                                                this.setState({ currentDay: OffsetDay((direction ? 1 : -1) * nbDays, this.state.currentDay) });
                                            }}
                                        />
                                            <Snackbar
                                                open={this.state.showSnack}
                                                autoHideDuration={6000}
                                                onClose={(event) => this.handleClose(event)}
                                                message={getStatusMessage(this.state.cached, this.props.error)}
                                            />
                                        </>)}

                            </div>

                            <Typography component="div" sx={{ mt: '2em' }}
                                className={'footer'}>Version {process.env.REACT_APP_VERSION} | <a
                                    href={'https://gitlab.com/nilsponsard/oh-my-ade'}>Code source</a></Typography>
                        </div>
                    </div>
                </Box>

            </ThemeProvider >


        );
    }
}


export default App;
