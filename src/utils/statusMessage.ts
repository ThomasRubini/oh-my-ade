export function getStatusMessage(cached: boolean, error: boolean): string {

    let loadStatus = '';
    if (cached) {
        let cacheDate = localStorage.getItem("last_fetch_date");
        let date = '';
        if (cacheDate) {
            date = ' du ' + new Date(cacheDate).toLocaleString('fr', {
                day: 'numeric',
                month: 'numeric',
                hour: '2-digit',
                minute: '2-digit'
            });

        }
        if (error)
            loadStatus = 'Erreur, affichage d’une version en cache' + date;
        else
            loadStatus = 'Affichage d’une version en cache' + date;
    } else {
        if (error)
            loadStatus = 'Erreur lors du chargement';
        else
            loadStatus = 'Nouvelle version chargée';
    }
    return loadStatus;
}